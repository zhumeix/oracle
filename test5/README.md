班级：20软工2班
学号：202010414230
姓名：祝梅


# 实验5：包，过程，函数的用法

实验目的

了解PL/SQL语言结构
了解PL/SQL变量和常量的声明和使用方法
学习包，过程，函数的用法。


# 实验内容

## 以hr用户登录
创建一个包(Package)，包名是MyPack。
在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：


SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
![1](包创建成功.png)

![2](程序包和包体创建成功.png)

输出的脚本代码：
create or replace PACKAGE MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/



测试


函数Get_SalaryAmount()测试方法：
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;

输出：
DEPARTMENT_ID DEPARTMENT_NAME		     SALARY_TOTAL
------------- ------------------------------ ------------
	   10 Administration			     4540
	   20 Marketing 			    19280
	   30 Purchasing			    25740
	   40 Human Resources			     6640
	   50 Shipping				   162700
	   60 IT				    29500
	   70 Public Relations			    10140
	   80 Sales				   309260
	   90 Executive 			    58420
	  100 Finance				    52448
	  110 Accounting			    20588

DEPARTMENT_ID DEPARTMENT_NAME		     SALARY_TOTAL
------------- ------------------------------ ------------
	  120 Treasury
	  130 Corporate Tax
	  140 Control And Credit
	  150 Shareholder Services
	  160 Benefits
	  170 Manufacturing
	  180 Construction
	  190 Contracting
	  200 Operations
	  210 IT Support
	  220 NOC

DEPARTMENT_ID DEPARTMENT_NAME		     SALARY_TOTAL
------------- ------------------------------ ------------
	  230 IT Helpdesk
	  240 Government Sales
	  250 Retail Sales
	  260 Recruiting
	  270 Payroll

![3](函数测试方法1.png)
过程Get_Employees()测试代码：
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/

输出：
101 Neena
    108 Nancy
        109 Daniel
        110 John
        111 Ismael
        112 Jose Manuel
        113 Luis
    200 Jennifer
    203 Susan
    204 Hermann
    205 Shelley
        206 William


![4](过程测试成功.png)
实验结果分析：本次实验新创建了一个程序包和包体，在这个过程中学到了了很多，最开始没有搞清楚程序包和包体之间的关系，还弄了很久，创建成功之后整个思路清晰了很多，所以本次实验是成功的。

[1]:base64







