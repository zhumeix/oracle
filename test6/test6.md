# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 


## 实验要求
设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
设计权限及用户分配方案。至少两个用户。
在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
设计一套数据库的备份方案。
## 实验步骤
### 1.表空间设计
 CREATE TABLESPACE ts_data

DATAFILE 'ts_data.dbf' SIZE 100M

AUTOEXTEND ON NEXT 50M

MAXSIZE UNLIMITED;


CREATE TABLESPACE ts_index

DATAFILE 'ts_index.dbf' SIZE 50M

AUTOEXTEND ON NEXT 20M

MAXSIZE UNLIMITED;



 ### 2.表设计
 - 商品信息表（product_info）：用于存储商品的基本信息，包括商品ID、商品名称、商品价格、商品库存等字段。 - 订单信息表（order_info）：用于存储订单的基本信息，包括订单ID、用户ID、订单状态、订单金额等字段。 - 用户信息表（user_info）：用于存储用户的基本信息，包括用户ID、用户名、密码、联系方式等字段。

- 订单详情表（order_detail）：用于存储订单的详细信息，包括订单ID、商品ID、购买数量、商品单价等字段。

产品信息表

CREATE TABLE product_info (

  product_id NUMBER(10) PRIMARY KEY,

  product_name VARCHAR2(100),

  product_price NUMBER(10, 2),

  product_stock NUMBER(10)

) TABLESPACE USERS;

订单信息表

CREATE TABLE order_info (

  order_id NUMBER(10) PRIMARY KEY,

  user_id NUMBER(10),

  order_status VARCHAR2(20),

  order_amount NUMBER(10, 2)

) TABLESPACE USERS;

用户信息表

CREATE TABLE user_info (

  user_id NUMBER(10) PRIMARY KEY,

  username VARCHAR2(50),

  password VARCHAR2(50),

  contact VARCHAR2(50)

) TABLESPACE USERS;




订单信息表

CREATE TABLE order_detail (

  order_id NUMBER(10),

  product_id NUMBER(10),

  quantity NUMBER(10),

  unit_price NUMBER(10, 2),

  PRIMARY KEY (order_id, product_id)

) TABLESPACE USERS;


### 3.权限及用户分配方案
权限及用户分配方案： - 创建管理员用户（admin_user）和普通用户（normal_user）两个用户。

-- 创建管理员用户
CREATE USER admin_user IDENTIFIED BY password;

GRANT CONNECT, RESOURCE, DBA TO admin_user;

-- 创建普通用户

CREATE USER normal_user IDENTIFIED BY password;

GRANT CONNECT, RESOURCE TO normal_user;

-- 给管理员用户授权

GRANT ALL PRIVILEGES TO admin_user;

-- 给普通用户授权

GRANT SELECT, INSERT, UPDATE, DELETE ON product_info TO normal_user;

GRANT SELECT, INSERT, UPDATE, DELETE ON order_info TO normal_user;


### 4.程序包设计
 - 创建一个名为order_pkg的程序包，包含以下存储过程和函数： - create_order：用于创建订单，接收用户ID和商品ID列表作为参数，返回订单ID。 - cancel_order：用于取消订单，接收订单ID作为参数，返回操作结果。 - get_order_info：用于获取订单信息，接收订单ID作为参数，返回订单的详细信息。 - get_user_order：用于获取用户的订单信息，接收用户ID作为参数，返回该用户的所有订单信息。 - get_product_info：用于获取商品信息，接收商品ID作为参数，返回商品的详细信息。


 ### 5.数据库备份方案
 - 每天定时备份数据库，并将备份文件存储在独立的服务器上，以防止数据丢失。
 - 定期测试备份文件的可用性，以确保在数据丢失的情况下能够及时恢复数据。
 BEGIN

  DBMS_SCHEDULER.CREATE_JOB (

    job_name        => 'backup_job',

    job_type        => 'BACKUP_SCRIPT',

    job_action      => 'backup_database.sh',

    start_date      => SYSTIMESTAMP,

    repeat_interval => 'FREQ=DAILY',

    enabled         => TRUE,

    comments        => 'Daily database backup'

  );
  
END;
/