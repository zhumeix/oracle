
CREATE OR REPLACE PACKAGE order_pkg AS
  -- 创建订单
  FUNCTION create_order(user_id IN INT, product_list IN VARCHAR) RETURN INT;
  -- 取消订单
  FUNCTION cancel_order(order_id IN INT) RETURN VARCHAR;
  -- 获取订单信息
  FUNCTION get_order_info(order_id IN INT) RETURN SYS_REFCURSOR;
  -- 获取用户订单信息
  FUNCTION get_user_order(user_id IN INT) RETURN SYS_REFCURSOR;
  -- 获取商品信息
  FUNCTION get_product_info(product_id IN INT) RETURN SYS_REFCURSOR;
END order_pkg;
/

CREATE OR REPLACE PACKAGE BODY order_pkg AS
  -- 创建订单
  FUNCTION create_order(user_id IN INT, product_list IN VARCHAR) RETURN INT AS
    order_id INT;
    order_amount DECIMAL(10,2) := 0;
    product_id INT;
    quantity INT;
    unit_price DECIMAL(10,2);
  BEGIN
    -- 生成订单ID
    SELECT MAX(order_id) + 1 INTO order_id FROM order_info;
    -- 插入订单信息
    INSERT INTO order_info (order_id, user_id, order_status, order_amount)
    VALUES (order_id, user_id, '待支付', order_amount);
    -- 解析商品列表
    FOR i IN 1..LENGTH(product_list) LOOP
      IF SUBSTR(product_list, i, 1) = ',' THEN
        product_id := TO_NUMBER(SUBSTR(product_list, 1, i-1));
        quantity := TO_NUMBER(SUBSTR(product_list, i+1));
        -- 查询商品单价
        SELECT product_price INTO unit_price FROM product_info WHERE product_id = product_id;
        -- 更新订单金额
        order_amount := order_amount + unit_price * quantity;
        -- 插入订单详情
        INSERT INTO order_detail (order_id, product_id, quantity, unit_price)
        VALUES (order_id, product_id, quantity, unit_price);
        -- 更新商品库存
        UPDATE product_info SET product_stock = product_stock - quantity WHERE product_id = product_id;
        -- 重置变量
        product_id := NULL;
        quantity := NULL;
        unit_price := NULL;
      END IF;
    END LOOP;
    -- 更新订单金额
    UPDATE order_info SET order_amount = order_amount WHERE order_id = order_id;
    -- 返回订单ID
    RETURN order_id;
  END create_order;

  -- 取消订单
  FUNCTION cancel_order(order_id IN INT) RETURN VARCHAR AS
    order_status VARCHAR(20);
  BEGIN
    -- 查询订单状态
    SELECT order_status INTO order_status FROM order_info WHERE order_id = order_id;
    -- 判断订单状态
    IF order_status = '待支付' THEN
      -- 删除订单详情
      DELETE FROM order_detail WHERE order_id = order_id;
      -- 删除订单信息
      DELETE FROM order_info WHERE order_id = order_id;
      -- 返回操作结果
      RETURN '订单已取消';
    ELSE
      -- 返回操作结果
      RETURN '订单无法取消';
    END IF;
  END cancel_order;

  -- 获取订单信息
  FUNCTION get_order_info(order_id IN INT) RETURN SYS_REFCURSOR AS
    order_info_cur SYS_REFCURSOR;
  BEGIN
    -- 查询订单信息
    OPEN order_info_cur FOR
      SELECT oi.order_id, oi.user_id, ui.username, oi.order_status, oi.order_amount
      FROM order_info oi
      JOIN user_info ui ON oi.user_id = ui.user_id
      WHERE oi.order_id = order_id;
    -- 返回结果集
    RETURN order_info_cur;
  END get_order_info;

  -- 获取用户订单信息
  FUNCTION get_user_order(user_id IN INT) RETURN SYS_REFCURSOR AS
    user_order_cur SYS_REFCURSOR;
  BEGIN
    -- 查询用户订单信息
    OPEN user_order_cur FOR
      SELECT oi.order_id, oi.order_status, oi.order_amount
      FROM order_info oi
      WHERE oi.user_id = user_id;
    -- 返回结果集
    RETURN user_order_cur;
  END get_user_order;

  -- 获取商品信息
  FUNCTION get_product_info(product_id IN INT) RETURN SYS_REFCURSOR AS
    product_info_cur SYS_REFCURSOR;
  BEGIN
    -- 查询商品信息
    OPEN product_info_cur FOR
      SELECT product_id, product_name, product_price, product_stock
      FROM product_info
      WHERE product_id = product_id;
    -- 返回结果集
    RETURN product_info_cur;
  END get_product_info;
END order_pkg;
/
