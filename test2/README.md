班级：20软工2班
学号：202010414230
姓名：祝梅
# 实验1：SQL语句的执行计划分析与优化指导

## 实验目的

  掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验数据库和用户

  数据库是pdborcl，用户是sys和hr

## 实验内容

- Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验步骤

- 第1步：以system登录到pdborcl，创建角色con_res_role和用户sale，并授权和分配空间：
CREATE ROLE zm1;
GRANT connect,resource,CREATE VIEW TO zm1;
CREATE USER zhumei2 IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER zhumei2 default TABLESPACE "USERS";

角色已创建。

ALTER USER zhumei2 QUOTA 50M ON users;

授权成功。

SQL> 
用户已创建。

SQL> 
用户已更改。

SQL> 
用户已更改。

SQL> GRANT zm TO zhumei2;




第二步：新用户sale连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。
连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> show user;
USER 为 "ZHUMEI2"
SELECT * FROM session_privs;
SELECT * FROM session_roles;

PRIVILEGE
----------------------------------------
CREATE SESSION
CREATE TABLE
CREATE CLUSTER
CREATE VIEW
CREATE SEQUENCE
CREATE PROCEDURE
CREATE TRIGGER
CREATE TYPE
CREATE OPERATOR
CREATE INDEXTYPE
SET CONTAINER

已选择 11 行。

SQL> 
ROLE
--------------------------------------------------------------------------------
ZM
CONNECT
RESOURCE
SODA_APP

CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;

表已创建。

SQL> 
已创建 1 行。

SQL> 
已创建 1 行。

SQL> 
视图已创建。

SQL> 
授权成功。

SQL> 
NAME
--------------------------------------------------
zhang
wang

第三步：用户hr连接到pdborcl，查询sale授予它的视图customers_view

$ sqlplus hr/123@pdborcl
SQL>  SELECT * FROM zhumei2.customers_view;

NAME
--------------------------------------------------
zhang
wang

#概要文件设置
上次成功登录时间: 星期日 4月  23 2023 10:47:58 +08:00

连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;

配置文件已更改
设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
锁定后，通过system用户登录，alter user zhumei2 unlock命令解锁。

SQL> exit
从 Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production 断开
[oracle@oracle1 ~]$ sqlplus system/123@pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期日 4月 23 11:11:10 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

上次成功登录时间: 星期日 4月  23 2023 11:10:19 +08:00

连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> alter user zhumei2 account unlock;

用户已更改。

查看数据库的使用情况
连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

TABLESPACE_NAME
------------------------------
FILE_NAME
--------------------------------------------------------------------------------
	MB     MAX_MB AUT
---------- ---------- ---
USERS
/home/oracle/app/oracle/oradata/orcl/pdborcl/users01.dbf
	 5 32767.9844 YES


SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
  8   where  a.tablespace_name = b.tablespace_name;

表空间名                           大小MB     剩余MB     使用MB    使用率%
------------------------------ ---------- ---------- ---------- ----------
SYSAUX				      370     24.375	345.625      93.41
UNDOTBS1			      100      38.75	  61.25      61.25
USERS					5      3.875	  1.125       22.5
SYSTEM				      260     8.0625   251.9375       96.9

实验结束删除用户和角色
SQL> drop role zm1;

角色已删除。

SQL> drop user zhumei2 cascade;

用户已删除。




## 实验分析及结果
实验二创建了角色和用户，并且赋予了用户权限的数据库表空间，还有概要文件密码登录错误次数的限制，最后实验完成后删除角色和用户，本次实验是成功的，没有发生一些配置上的问题，缺点是没有进行一些额外的扩展。
